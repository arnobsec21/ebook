       
        <!-- site onscroll web site view  -->
        <div class="sticky-init fixed-header common-sticky">
            <div class="container d-none d-lg-block">
                <div class="row align-items-center">
                    <div class="col-lg-3">
                        <a href="index.html" class="site-brand">
                                <img src="{{ asset('front_end/images/ebook-GIF100bold.gif') }} " alt="" height="60">
                        </a>
                    </div>
                    <div class="col-lg-6">
                        <div class="header-search-block">
                            <input type="text" placeholder="Search entire store here">
                            <button>Search</button>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="main-navigation flex-lg-right">
                            <div class="cart-widget">
                                <div class="login-block">
                                    <a href="login-register.html" class="font-weight-bold"><i class="fas fa-lock"></i> Login </a> <br>
                                </div>
                                <div class="cart-block">
                                    <div class="cart-total">
                                        <span class="text-number">
                                            1
                                        </span>
                                        <span class="text-item">
                                            Shopping Cart
                                        </span>
                                        <span class="price">
                                            £0.00
                                            <i class="fas fa-chevron-down"></i>
                                        </span>
                                    </div>
                                    <div class="cart-dropdown-block">
                                        <div class=" single-cart-block ">
                                            <div class="cart-product">
                                                <a href="product-details.html" class="image">
                                                    <img src="image/products/cart-product-1.jpg" alt="">
                                                </a>
                                                <div class="content">
                                                    <h3 class="title"><a href="product-details.html">Kodak PIXPRO
                                                            Astro Zoom AZ421 16 MP</a>
                                                    </h3>
                                                    <p class="price"><span class="qty">1 ×</span> £87.34</p>
                                                    <button class="cross-btn"><i class="fas fa-times"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class=" single-cart-block ">
                                            <div class="btn-block">
                                                <a href="cart.html" class="btn">View Cart <i
                                                        class="fas fa-chevron-right"></i></a>
                                                <a href="checkout.html" class="btn btn--primary">Check Out <i
                                                        class="fas fa-chevron-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-bottom">
                <div class="container">
                    <div class="row align-items-center">                        
                        <div class="col-lg-12">
                            <div class="main-navigation flex-lg-left">
                                <ul class="main-menu menu-left li-last-0">
                                    <li class="menu-item has-children mega-menu">
                                        <a href="javascript:void(0)">প্রকাশনী <i class="fas fa-chevron-down dropdown-arrow"></i></a>
                                        <ul class="sub-menu four-column">
                                            @if(!$publisher->isEmpty() && $publisher->count() == 20)
                                                    @foreach($publisher as $val)
                                                        <li class="cus-col-25"><a href="{{ route('details.publisher',$val->id) }}"> {{ $val->name_bangla}} </a></li>
                                                    @endforeach

                                                    <div  class="pt--10"  >
                                                        <a href="{{ route('all.publisher') }}" class="btn btn-outlined--primary " tabindex="-1">
                                                           See More
                                                        </a>
                                                    </div>
                                            @endif


                                        </ul>
                                    </li>
                                    <!-- Shop -->
                                    <li class="menu-item has-children mega-menu">
                                        <a href="javascript:void(0)">লেখক  <i class="fas fa-chevron-down dropdown-arrow"></i></a>
                                        <ul class="sub-menu four-column">

                                             @if(!$author->isEmpty() && $author->count() == 20)
                                                    @foreach($author as $val)
                                                        <li class="cus-col-25"><a href="{{ route('details.author',$val->id) }}"> {{ $val->name_bangla}}</a> </li>
                                                    @endforeach
                                                    <div  class="pt--10"  >
                                                        <a href="{{ route('all.author') }}" class="btn btn-outlined--primary " tabindex="-1">
                                                           See More
                                                        </a>
                                                    </div>
                                            @endif
                                           
                                            
                                        </ul>
                                    </li>
                                    <!-- Pages -->
                                    <li class="menu-item">
                                        <a href="{{ route('all.book') }}">সকল বই </a>
                                    </li>

                                  

                                    <li class="menu-item">
                                        <a href="#" class="price-old">বিষয়াবলী</a>
                                    </li>

                                    <li class="menu-item">
                                        <a href="#" class="price-old">বেস্ট সেলার বই</a>
                                    </li>
                                    <li class="menu-item">
                                        <a href="#" class="price-old">ভর্তি ও নিয়োগ পরীক্ষা</a>
                                    </li>

                                     <li class="menu-item">
                                        <a href="" class="price-old">অতিরিক্ত ছাড়ের বই</a>
                                    </li>                                    
                                    

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
