@extends('layouts.public_layout')

@section('content')
<section class="section-margin">
		<div class="container ">
		<div class="breadcrumb-contents light-gray-background pl-3">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb">
					<li class="breadcrumb-item">
						<h6>সমস্ত  প্রকাশনী</h6s>
					</li>
					{{-- </li> <a href="#" class="btn btn-outlined--red-faded left">See More</a></li> --}}
				</ol>
			</nav>
		</div>


	</div>
    <div class="container">

        <div class="shop-product-wrap with-pagination row space-db--30 shop-border grid-four">

        	@if(!$allpublisher->isEmpty())
        	@foreach($allpublisher as $val)
	       		<div class="col-lg-4 col-sm-6">
					<div class="product-card">
						<div class="product-grid-content">
							<a href="{{ route('details.publisher',$val->id) }}">
							<div class="product-card--body">
								<div class="card-image">
									<img src="{{ asset('publishers/images/'.$val->image_url) }}" alt="">
									
								</div>
								<div class="price-block">
									<span class="price">{{ $val->name_bangla}} </span>
									
								</div>
							</div>
							</a>
						</div>
					
					</div>
				</div>
			@endforeach
			@endif


		</div>

		<div class="row pt--30">
			<div class="col-md-12">
				<div class="pagination-block">

					 @if ($allpublisher->lastPage() > 1)
				        <ul class="pagination-btns flex-center">
				            <li class="{{ ($allpublisher->currentPage() == 1) ? ' disabled' : '' }} page-item">
				                <a class=" page-link " href="{{ $allpublisher->url(1) }}" aria-label="Previous">
				                    <span aria-hidden="true">&laquo;</span>
				                    <span class="sr-only">Previous</span>
				                </a>
				            </li>
				           {{ $allpublisher->links() }}
				            <li class="{{ ($allpublisher->currentPage() == $allpublisher->lastPage()) ? ' disabled' : '' }} page-item">
				                <a href="{{ $allpublisher->url($allpublisher->currentPage()+1) }}" class="page-link" aria-label="Next">
				                    <span aria-hidden="true">&raquo;</span>
				                    <span class="sr-only">Next</span>
				                </a>
				            </li>
				        </ul>
					@endif

{{-- 
					<ul class="pagination-btns flex-center">
						<li><a href="#" class="single-btn prev-btn ">|<i class="zmdi zmdi-chevron-left"></i> </a></li>
						<li><a href="#" class="single-btn prev-btn "><i class="zmdi zmdi-chevron-left"></i> </a></li>
						<li class="active"><a href="#" class="single-btn">1</a></li>
						<li><a href="#" class="single-btn">2</a></li>
						<li><a href="#" class="single-btn">3</a></li>
						<li><a href="#" class="single-btn">4</a></li>
						<li><a href="#" class="single-btn next-btn"><i class="zmdi zmdi-chevron-right"></i></a></li>
						<li><a href="#" class="single-btn next-btn"><i class="zmdi zmdi-chevron-right"></i>|</a></li>
					</ul> --}}
				</div>
			</div>
		</div>
    </div>
</section>
@endsection
