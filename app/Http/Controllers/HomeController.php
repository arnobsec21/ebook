<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Console\Command;
// use App\Models\Author\AuthorModel;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        

        $getAuthor = DB::table('tbl_author')->take(20)->get();
        $getPublisher = DB::table('tbl_publisher')->take(20)->get();


        $bestseller = DB::table('tbl_book')->take(16)->get();
        $popular = DB::table('tbl_book')->skip(16)->take(16)->get();
        $recent = DB::table('tbl_book')->take(16)->get();

        return view('front.pages.home.index')
                ->with('author',$getAuthor)
                ->with('publisher',$getPublisher)

                ->with('bestseller',$bestseller)
                ->with('popular',$popular)
                ->with('recent',$recent);

    }

    public function allauthor(Request $request){

        $getAuthor = DB::table('tbl_author')->take(20)->get();
        $getPublisher = DB::table('tbl_publisher')->take(20)->get();


        $allauthor = DB::table('tbl_author')->paginate(24);

        return view('front.pages.author.index')
            ->with('author',$getAuthor)

            ->with('publisher',$getPublisher)

            ->with('allauthor',$allauthor);

    }

    public function allpublisher(Request $reqest){

        $getAuthor = DB::table('tbl_author')->take(20)->get();
        $getPublisher = DB::table('tbl_publisher')->take(20)->get();
        $allpublisher = DB::table('tbl_publisher')->paginate(24);


        return view('front.pages.publisher.index')
            ->with('author',$getAuthor)
            ->with('publisher',$getPublisher)

            ->with('allpublisher',$allpublisher);

    }
    public function allbook(Request $reqest){

        $getAuthor = DB::table('tbl_author')->take(20)->get();
        $getPublisher = DB::table('tbl_publisher')->take(20)->get();

        $allbook = DB::table('tbl_book')->paginate(30);


        return view('front.pages.book.index')
            ->with('author',$getAuthor)
            ->with('publisher',$getPublisher)
            ->with('allbook',$allbook);

    }

    public function detailsbook(Request $request){

        $bookId = $request->id;
        // dd($bookId);
        $getAuthor = DB::table('tbl_author')->take(20)->get();
        $getPublisher = DB::table('tbl_publisher')->take(20)->get();

        $relatedProduct = DB::table('tbl_book')->take(8)->get();

        $bookDetails = DB::table('tbl_book')->where('id',$request->id)->get()->first();

        return view('front.pages.book.book_details')
            ->with('author',$getAuthor)
            ->with('publisher',$getPublisher)
            ->with('bookDetails',$bookDetails)
            ->with('relatedProduct',$relatedProduct);

    }
    public function detailspublisher(Request $request){

        $bookId = $request->id;
        // dd($bookId);
        $getAuthor = DB::table('tbl_author')->take(20)->get();
        $getPublisher = DB::table('tbl_publisher')->take(20)->get();

        $relatedProduct = DB::table('tbl_book')->take(16)->get();

        $publisherDetails = DB::table('tbl_publisher')->where('id',$request->id)->get()->first();

        return view('front.pages.publisher.publisher_details')
            ->with('author',$getAuthor)
            ->with('publisher',$getPublisher)
            ->with('publisherDetails',$publisherDetails)
            ->with('relatedProduct',$relatedProduct);

    }
    public function detailsauthor(Request $request){

        $bookId = $request->id;
        // dd($bookId);
        $getAuthor = DB::table('tbl_author')->take(20)->get();
        $getPublisher = DB::table('tbl_publisher')->take(20)->get();

        $relatedProduct = DB::table('tbl_book')->take(16)->get();

        $authorDetails = DB::table('tbl_author')->where('id',$request->id)->get()->first();

        return view('front.pages.author.author_details')
            ->with('author',$getAuthor)
            ->with('publisher',$getPublisher)
            ->with('authorDetails',$authorDetails)
            ->with('relatedProduct',$relatedProduct);

    }

    
}
